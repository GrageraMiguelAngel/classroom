//
//  main.c
//  Seminario_0
//
//  Created by Miguel Ángel Gragera García on 13/10/2019.
//  Copyright © 2019 Miguel Ángel Gragera García. All rights reserved.
//
#include <stdlib.h>
#include <stdio.h>
#define N 128
#define M 128
#define N_ITERA 10
void init2(float *v_a, float *v_b);
void jacobi(float *in, float *out);
int main(int argc, const char * argv[]) {
    
    float *v_a, *v_b;
    int i,j;
    v_a = (float *)malloc(N*M*sizeof(float));
    if(v_a == NULL)
        return -1;
    v_b = (float *)malloc(N*M*sizeof(float));
    if(v_b== NULL)
        return -1;
    int pos =0;
    float *aux = NULL;
    init2(v_a, v_b);
    
    for(i =0; i < N_ITERA ; ++i){
        jacobi(v_a,v_b);
        aux = v_a;
        v_a = v_b;
        v_b = aux;
    }
    
    for(i = 0; i < N ; ++i){
        for(j = 0; j < M; ++j){
         pos = i*M +j;
            printf(" %f ",v_b[pos]);
        }
    }
    printf("\n");

    
    free(v_a);
    free(v_b);

    
    return 0;
}
void init2(float *v_a,float *v_b){
    
    for (int i = 0; i<N; ++i){
        for(int j = 0; j < M ; ++j){
            int pos = i*M + j;//Transforma 2D a 1D 7*8 + 7 = 63 [0,7][0,7] = [0-63]
            v_a[pos] = v_b[pos] =(i == 0 || i == N-1 || j ==0 || j == M -1)?150.0f:70.0f;
            
        }
    }
}
void jacobi(float *in, float *out){
    for (int i = 0; i<N; ++i){
           for(int j = 1; j < (M-1) ; ++j){
               int pos = i*M + j;
               out[pos] = 0.2f *(in[pos] + in[pos-1] + in[pos +1] + in[pos + M] + in[pos - M]);
               
           }
       }
    
}

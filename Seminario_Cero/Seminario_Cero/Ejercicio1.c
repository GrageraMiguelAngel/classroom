//
//  Ejercicio1.c
//  Seminario_Cero
//
//  Created by Miguel Ángel Gragera García on 13/10/2019.
//  Copyright © 2019 Miguel Ángel Gragera García. All rights reserved.
//

#include "Ejercicio1.h"
#define N 1200

void ejercicio1(){
    //Stencil 1 dimension
    float *a;
    a = (float *)malloc(N*sizeof(float));
    int i;
    
    for (i = 0; i < N; i++){
        a[i] = 1;
        
    }
    
    for(i = 1; i< N-1; i++){
        
        a[i] = 0.3 * (a[i-1] + a[i] + a[i+1]);
    }
    
    for (i = 0; i < N; i++){
        printf(" %f ", a[i]);
        
    }
}
